"""
Test if the grid is correct (e.g. no adjacent black cells and all non-black cells form
a single orthogonally connected group)

Arguments
- t: array of size n*n with values in [0, n] (0 if the cell is empty = black)
"""
function isGridFeasible(t::Matrix{Int})
	n = size(t, 1)

	# Check that black cells are not adjacent
	for i in 2:n
		for j in 1:n
			if t[i, j] == 0 && t[i-1, j] == 0
				return false
			end
		end
	end

	for i in 1:n
		for j in 2:n
			if t[i, j] == 0 && t[i, j-1] == 0
				return false
			end
		end
	end

	# Non-black cells should form orthogonally connected group

	n = size(t, 1)
	groups = zeros(Int8, n, n)
	maxGroup = 0

	for i in 1:n
		for j in 1:n
			if t[i, j] == 0
				continue
			end

			# Left neighbour has group assigned
			if j >= 2 && groups[i, j-1] > 0
				groups[i, j] = groups[i, j-1]
				# Top neighbour has group assigned
			elseif i >= 2 && groups[i-1, j] > 0
				groups[i, j] = groups[i-1, j]
				# Assign a new group
			else
				maxGroup += 1
				groups[i, j] = maxGroup
			end
		end
	end

	# Run for the second time to merge adjacent orthogonal groups
	for i in reverse(1:n)
		for j in reverse(1:n)
			if t[i, j] == 0
				continue
			end

			# Left neighbour has a different group assigned
			if j >= 2 && groups[i, j-1] > 0 && groups[i, j] != groups[i, j-1]
				C = findall(groups .== groups[i, j])
				groups[C] .= groups[i, j-1]
				# Top neighbour has a different group assigned
			elseif i >= 2 && groups[i-1, j] > 0 && groups[i, j] != groups[i-1, j]
				C = findall(groups .== groups[i, j])
				groups[C] .= groups[i-1, j]
			end
		end
	end

	uniqueGroups = unique(groups)
	uniqueGroups = filter(e -> e != 0, uniqueGroups)

	# There should be only one orthogonally connected group
	return size(uniqueGroups, 1) == 1
end

