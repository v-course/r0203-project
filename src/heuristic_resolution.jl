include("feasibility.jl");


"""
Heuristically solve a grid by successively assigning values to one of the most constrained cells 
(i.e., a cell in which the number of remaining possible values is the lowest)

Argument
- t: array of size n*n with values in [1, n]

Return
- gridFeasible: true if the problem is solved
- tCopy: the grid solved (or partially solved if the problem is not solved),
		 0 corresponds to no value (black cell)
"""
function heuristicSolve(t::Matrix{Int}, timeLimitMs::Int)

	n = size(t, 1)

	cellsWithSameValues = []

	# For each row, determine coordinates of cells that have the same value.
	# Only one of these cells can be left clear in the final solution
	for i in 1:n
		alreadyChecked = []
		for j in 1:n
			if !(j in alreadyChecked)
				group = []
				for k in 1:n
					if k != j && t[i, k] == t[i, j] # Same values detected
						push!(group, (i, j))
						push!(group, (i, k))
						push!(alreadyChecked, k)
					end
				end

				if size(group, 1) > 0
					push!(cellsWithSameValues, group)
				end
			end
		end
	end

	# For each column, determine coordinates of cells that have the same value.
	# Only one of these cells can be left clear in the final solution
	for j in 1:n
		alreadyChecked = []
		for i in 1:n
			if !(i in alreadyChecked)
				group = []
				for k in 1:n
					if k != i && t[k, j] == t[i, j] # Same values detected
						push!(group, (i, j))
						push!(group, (k, j))
						push!(alreadyChecked, k)
					end
				end

				if size(group, 1) > 0
					push!(cellsWithSameValues, group)
				end
			end
		end
	end

	nGroups = size(cellsWithSameValues, 1)
	maxNumberOfCombinations = 2^nGroups

	resolutionTime = 0

	for c in 1:maxNumberOfCombinations
		startingTime = time()

		indexesToCrossOut = toBitsArray(c, nGroups)

		tCopy = copy(t)

		for i in 1:size(cellsWithSameValues, 1)
			# Adding 1 as indexesToCrossOut have either 0 or 1, and we need either 1 or 2
			cellToCrossOut = cellsWithSameValues[i][indexesToCrossOut[i]+1]
			tCopy[cellToCrossOut[1], cellToCrossOut[2]] = 0
		end

		if isGridFeasible(tCopy)
			return true, tCopy
		end

		resolutionTime += time() - startingTime

		if resolutionTime > timeLimitMs / 1000
			return false, tCopy
		end
	end

	# No solution found, grid is not feasible
	return false, copy(t)
end

function toBitsArray(number::Integer, maxLength::Integer)
	return parse.(Int, reverse(split(bitstring(number), ""))[1:maxLength])
end