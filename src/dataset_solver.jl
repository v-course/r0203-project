include("cplex_resolution.jl");
include("heuristic_resolution.jl");
include("generation.jl");


"""
Solve all the instances contained in "../data" through CPLEX and the heuristic

The results are written in "../res/cplex" and "../res/heuristic"

Remark: If an instance has previously been solved (either by cplex or the heuristic) it will not be solved again
"""
function solveDataSet()

	dataFolder = "../data/"
	resFolder = "../res/"

	resolutionMethod = ["cplex", "heuristique"]
	resolutionFolder = resFolder .* resolutionMethod

	for folder in resolutionFolder
		if !isdir(folder)
			mkdir(folder)
		end
	end

	global isOptimal = false
	global solveTime = -1

	# For each input file
	# (for each file in folder dataFolder which ends by ".txt")
	for file in filter(x -> occursin(".txt", x), readdir(dataFolder))

		println("-- Resolution of ", file)
		t = readInputFile(dataFolder * file)

		for methodId in eachindex(resolutionMethod)

			outputFile = resolutionFolder[methodId] * "/" * file

			# If the input file has not already been solved by this method
			if !isfile(outputFile)

				fout = open(outputFile, "w")

				resolutionTime = -1
				isOptimal = false

				# If the method is cplex
				if resolutionMethod[methodId] == "cplex"
					isOptimal, x, resolutionTime = cplexSolve(t)
					println("cplex <<<")

					println(isOptimal)
					displayGrid(x)

					# Also write the solution (if any)
					if isOptimal
						writeSolution(fout, x)
					end

					# If the method is heuristic
				else
					isOptimal = false
					solution = []

					# Start a chronometer 
					startingTime = time()

					println("solving...")

					# Set a limit of 60_000 ms = 1 min
					isOptimal, solution = heuristicSolve(t, 60_000)

					# Stop the chronometer
					resolutionTime = time() - startingTime

					println()
					displayGrid(solution)

					# Write the solution (if any)
					if isOptimal
						writeSolution(fout, solution)
					end
				end

				println(fout, "solveTime = ", resolutionTime)
				println(fout, "isOptimal = ", isOptimal)
				close(fout)
			end


			# Display the results obtained with the method on the current instance
			include(outputFile)
			println(resolutionMethod[methodId], " optimal: ", isOptimal)
			println(resolutionMethod[methodId], " time: " * string(round(solveTime, sigdigits = 2)) * "s\n")
		end
	end
end

