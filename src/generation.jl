# This file contains methods to generate a data set of instances (i.e., sudoku grids)
include("io.jl")
include("cplex_resolution.jl")

"""
Generate an n*n grid. Maximal number that can be present in the grid equals to n.

Argument
- n: size of the grid
"""
function generateInstance(n::Int64)

	# True if has a solution
	isGridValid = false

	t = []

	while !isGridValid

		# Array that will contain the generated grid
		t = Matrix{Int}(zeros(n, n))

		for i in 1:n
			for j in 1:n
				allowedValues = getPossibleValuesForCell(t, i, j)

				if size(allowedValues, 1) == 0
					isGridValid = false
					break
				end

				t[i, j] = rand(allowedValues)
				isGridValid = true

			end
		end

		# Check for feasibility
		try
			isOptimal, _, __ = cplexSolve(t)
		catch e
			isGridValid = false
		end

	end

	return t
end

"""
Generate all the instances

Remark: a grid is generated only if the corresponding output file does not already exist
"""
function generateDataSet()

	# For each grid size considered
	for size in [5, 6, 8, 10, 12]

		# Generate 10 instances
		for instance in 1:10

			fileName = "../data/instance_t" * string(size) * "_" * string(instance) * ".txt"

			if !isfile(fileName)
				println("-- Generating file " * fileName)
				saveInstance(generateInstance(size), fileName)
			end
		end
	end
end

function getPossibleValuesForCell(t, i, j)
	alreadyPlacedTwice = []

	n = size(t, 1)

	for c in 1:n
		for k in 1:n
			if k != c && t[i, c] == t[i, k]
				push!(alreadyPlacedTwice, t[i, c])
			end
		end
	end

	for r in 1:n
		for k in 1:n
			if k != r && t[r, j] == t[k, j]
				push!(alreadyPlacedTwice, t[r, j])
			end
		end
	end

	allowedValues = []

	for k in 1:n
		if !(k in alreadyPlacedTwice)
			push!(allowedValues, k)
		end
	end

	return allowedValues
end
