using CPLEX
include("feasibility.jl");


TOL = 0.00001


function cplexSolveWithForbiddenSolutions(t::Matrix{Int}, forbiddenSolutions::Vector{Matrix{Int}})

	n = size(t, 1)

	# Create the model
	m = Model(CPLEX.Optimizer)

	# x[i, j, k] = 1 if cell (i, j) has value k
	@variable(m, x[1:n, 1:n, 1:n], Bin)

	# y[i, j] = 1 if cell (i, j) is black
	@variable(m, y[1:n, 1:n], Bin)

	# Set the fixed value in the grid
	for l in 1:n
		for c in 1:n
			for k in 1:n
				if k == t[l, c]
					# If cell (l, c) is black (which means y(l, c) = 1), it does not have any value
					@constraint(m, x[l, c, k] == 1 - y[l, c])
				else
					@constraint(m, x[l, c, k] == 0)
				end
			end
		end
	end

	# Each line l has one cell with value k
	@constraint(m, [k in 1:n, l in 1:n], sum(x[l, j, k] for j in 1:n) <= 1)

	# Each column c has one cell with value k
	@constraint(m, [k in 1:n, c in 1:n], sum(x[i, c, k] for i in 1:n) <= 1)

	# Black cells cannot be adjacent
	@constraint(m, [i in 1:n, j in 2:n], (y[i, j] + y[i, j-1]) <= 1) # Constraint over row
	@constraint(m, [i in 2:n, j in 1:n], (y[i, j] + y[i-1, j]) <= 1) # Constraint over column

	# Add constraints to exclude forbidden solutions
	for k in eachindex(forbiddenSolutions)
		# Solution is a matrix (n x n) where (i,j) = 0 if a cell is black
		# So we want forbid that particular combination of black cells
		s = forbiddenSolutions[k]

		@constraint(m, sum((1 - y[i, j]) for i in 1:n, j in 1:n if s[i, j] == 0) >= 1)
	end

	# Objective function is a constant value
	@objective(m, Max, 1)

	start = time()
	optimize!(m)

	# Returns:
	# - a boolean which is true if a feasible solution is found (type: Bool)
	# - the value of each cell (type: Matrix{VariableRef})
	# - the resolution time (type Float64)
	return primal_status(m) == MOI.FEASIBLE_POINT, x, time() - start
end


# Return:
# - a boolean which is true if a feasible solution is found (type: Bool)
# - the value of each cell (type: Matrix{Int})
# - the resolution time (type Float64)
function cplexSolve(t::Matrix{Int})
	solutionsTried = Vector{Matrix{Int}}()

	totalResolutionTime = 0

	isOptimal, x, resolutionTime = cplexSolveWithForbiddenSolutions(t, solutionsTried)
	m = toBinaryMatrix(x)
	totalResolutionTime += resolutionTime

	while !isGridFeasible(m)
		push!(solutionsTried, m)
		isOptimal, x, resolutionTime = cplexSolveWithForbiddenSolutions(t, solutionsTried)
		m = toBinaryMatrix(x)
		totalResolutionTime += resolutionTime
	end

	return isOptimal, toMatrix(x), totalResolutionTime
end


function toMatrix(x::Array{VariableRef, 3})
	n = size(x, 1)

	t = zeros(Int, n, n)

	for l in 1:n
		for c in 1:n
			for k in 1:n
				if JuMP.value(x[l, c, k]) > TOL
					t[l, c] = k
				end
			end
		end
	end

	return t
end

function toBinaryMatrix(x::Array{VariableRef, 3})
	n = size(x, 1)

	t = zeros(Int, n, n)

	for l in 1:n
		for c in 1:n
			for k in 1:n
				if JuMP.value(x[l, c, k]) > TOL
					t[l, c] = 1
				end
			end
		end
	end

	return t
end