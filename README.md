# Singles

https://www.chiark.greenend.org.uk/~sgtatham/puzzles/js/singles.html

Pour utiliser ce programme, se placer dans le répertoire ./src

## Utilisation

Les utilisations possibles sont les suivantes :

### I - Génération d'un jeu de données

```
julia
include("generation.jl")
generateDataSet()
```

### II - Résolution du jeu de données

```
julia
include("dataset_solver.jl")
solveDataSet()
```

### III - Présentation des résultats sous la forme d'un tableau

```
julia
include("io.jl")
resultsArray("../res/array.tex")
```
